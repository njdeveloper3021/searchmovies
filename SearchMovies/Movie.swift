//
//  Movie.swift
//  SearchMovies
//
//  Created by Navya Jagadish on 6/11/17.
//  Copyright © 2017 NavyaJagadish. All rights reserved.
//

import Foundation

struct Movie {
    var adult : Bool?
    var backdrop_path : String?
    var id : Int?
    var original_language : String?
    var original_title : String?
    var overview : String?
    var poster_path : String?
    var release_date : String?
    var title : String?
    var popularity : String?
    var vote_average : String?
    var vote_count : Int?
}
