//
//  MovieDetailViewController.swift
//  SearchMovies
//
//  Created by Navya Jagadish on 6/12/17.
//  Copyright © 2017 NavyaJagadish. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    var selectedMovie = Movie()
    
    @IBOutlet weak var backDropImg: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieOverview: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateDisplay()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: - Private Methods
    func updateDisplay() {
        if selectedMovie.backdrop_path != nil {
            let urlString = NSString(format: "%@%@", kImageBaseURL, selectedMovie.backdrop_path!)
            let url = URL(string: urlString as String)
            let data = try? Data(contentsOf: url!)
            backDropImg.image = UIImage(data: data!)
        } else {
            backDropImg.image = UIImage(named: "question")
        }
        
        movieTitle.text = selectedMovie.original_title
        movieOverview.text = selectedMovie.overview
        releaseDate.text = selectedMovie.release_date
    }
}
