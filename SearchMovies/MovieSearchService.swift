//
//  MovieSearchService.swift
//  SearchMovies
//
//  Created by Navya Jagadish on 6/11/17.
//  Copyright © 2017 NavyaJagadish. All rights reserved.
//

import Foundation
import Alamofire

protocol MovieSearchServiceDelegate : class {
    func searchRetrieved(movies: [Movie])
    func searchFailed(errorMessage: String)
}

class MovieSearchService {
    weak var delegate : MovieSearchServiceDelegate?
    
    func getMovies(parameters:Parameters!) {
        let urlStr = NSString(format: "%@", kBaseURL)
        
        Alamofire.request(urlStr as String, method:.post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseString { (response:DataResponse) in
            
            if let data = response.result.value?.data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
                    let results = json?["results"] as! NSArray
                    print(results)
                    var searchMovies = [Movie]()
                    for item in results {
                        var movie = Movie()
                        movie.adult = (item as AnyObject)["adult"] as? Bool
                        movie.id = (item as AnyObject)["id"] as? Int
                        movie.backdrop_path = (item as AnyObject)["backdrop_path"] as? String
                        movie.original_language = (item as AnyObject)["original_language"] as? String
                        movie.original_title = (item as AnyObject)["original_title"] as? String
                        movie.overview = (item as AnyObject)["overview"] as? String
                        movie.popularity = (item as AnyObject)["popularity"] as? String
                        movie.poster_path = (item as AnyObject)["poster_path"] as? String
                        movie.release_date = (item as AnyObject)["release_date"] as? String
                        movie.title = (item as AnyObject)["title"] as? String
                        movie.vote_average = (item as AnyObject)["vote_average"] as? String
                        movie.vote_count = (item as AnyObject)["vote_count"] as? Int
                        
                        searchMovies.append(movie)
                    }
                    self.delegate?.searchRetrieved(movies: searchMovies)
                } catch {
                    self.delegate?.searchFailed(errorMessage: (response.error?.localizedDescription)!)
                }
            }
            
        }
    }
}
