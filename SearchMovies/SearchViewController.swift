//
//  SearchViewController.swift
//  SearchMovies
//
//  Created by Navya Jagadish on 6/11/17.
//  Copyright © 2017 NavyaJagadish. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MovieSearchServiceDelegate, UISearchResultsUpdating, UISearchBarDelegate {
    @IBOutlet weak var myTableView: UITableView!
    
    var filterdResult: [Movie] = []
    var searchQuery: NSString!
    let movieSearchService = MovieSearchService()
    var searchController: UISearchController!
    var showSearchResult = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchQuery = ""
        movieSearchService.delegate = self
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = true
        searchController.searchBar.placeholder = "Search by name..."
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        myTableView.tableHeaderView = searchController.searchBar
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showSearchResult {
            return self.filterdResult.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if showSearchResult {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SearchTableViewCell
            
            cell.movieName.text = filterdResult[indexPath.row].original_title
            if filterdResult[indexPath.row].poster_path != nil {
                let urlString = NSString(format: "%@%@", kImageBaseURL, filterdResult[indexPath.row].poster_path!)
                let url = URL(string: urlString as String)
                let data = try? Data(contentsOf: url!)
                cell.movieImg.image = UIImage(data: data!)
            } else {
                cell.movieImg.image = UIImage(named: "question")
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SearchTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movieDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        movieDetailVC.selectedMovie = filterdResult[indexPath.row]
        self.navigationController?.pushViewController(movieDetailVC, animated: true)
    }
    
    //MARK: - Filtering Results
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !showSearchResult {
            showSearchResult = true
            let queryString = searchBar.text
            if queryString != "" {
                let params:[String:String] = ["query":queryString!]
                movieSearchService.getMovies(parameters: params)
            }
        }
        searchController.searchBar.resignFirstResponder()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if !showSearchResult {
            showSearchResult = true
        }
        let queryString = searchController.searchBar.text
        if queryString != "" {
            let params:[String:String] = ["query":queryString!]
            movieSearchService.getMovies(parameters: params)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        showSearchResult = false
        self.myTableView.reloadData()
    }

    //MARK: - MovieSearchServiceDelegate Methods
    func searchRetrieved(movies: [Movie]) {
        self.filterdResult = movies
        self.myTableView.reloadData()
    }
    
    func searchFailed(errorMessage: String) {
        print(errorMessage)
    }

}
